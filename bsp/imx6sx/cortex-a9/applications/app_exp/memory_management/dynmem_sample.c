/*
 * File: dynmem_sample.c
 * Created Date: 2020-05-20
 * Author: David
 * Contact: <david_qv@163.com>
 * 
 * Last Modified: Wednesday May 20th 2020 2:12:28 pm
 * 
 * Copyright (c) 2020 NONE
 * It is never too late to be what you might have been.
 * -----
 * HISTORY:
 * Date      	 By	Comments
 */


#include <rtthread.h>

#define THREAD_PRIORITY 25
#define THREAD_STACK_SIZE 512
#define THREAD_TIMESLICE 5

/* 线程入口*/
void thread1_entry(void *parameter)
{
    rt_int32_t i;
    rt_int8_t *ptr = RT_NULL; /* 内存块的指针*/

    for (i = 0; ; i++) {
        /* 每次分配(1 << i) 大小字节数的内存空间*/
        ptr = rt_malloc(1 << i);
        /* 如果分配成功*/
        if (ptr != RT_NULL) {
            rt_kprintf("get memory :%d byte\n", (1 << i));
            /* 释放内存块*/
            rt_free(ptr);
            rt_kprintf("free memory :%d byte\n", (1 << i));
            ptr = RT_NULL;
        } else {
            rt_kprintf("try to get %d byte memory failed!\n", (1 << i));
            break;
        }
    }
}

int dynmem_sample(void)
{
    rt_thread_t tid = RT_NULL;

    /* 创建线程1 */
    tid = rt_thread_create("thread1",
                            thread1_entry, RT_NULL,
                            THREAD_STACK_SIZE,
                            THREAD_PRIORITY,
                            THREAD_TIMESLICE);
    if (tid != RT_NULL) {
        rt_thread_startup(tid);
    }

    return 0;
}

MSH_CMD_EXPORT(dynmem_sample, dynmem sample);