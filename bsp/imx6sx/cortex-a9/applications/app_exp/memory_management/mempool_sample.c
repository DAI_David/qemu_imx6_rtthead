/*
 * File: mempool_sample.c
 * Created Date: 2020-05-20
 * Author: David
 * Contact: <david_qv@163.com>
 * 
 * Last Modified: Wednesday May 20th 2020 2:35:13 pm
 * 
 * Copyright (c) 2020 NONE
 * It is never too late to be what you might have been.
 * -----
 * HISTORY:
 * Date      	 By	Comments
 */


#include <rtthread.h>

static rt_uint8_t *ptr[50]={RT_NULL};
static rt_uint8_t mempool[4096];
static struct rt_mempool mp;

#define THREAD_PRIORITY 25
#define THREAD_STACK_SIZE 512
#define THREAD_TIMESLICE 5

/* 指向线程控制块的指针*/
static rt_thread_t tid1 = RT_NULL;
static rt_thread_t tid2 = RT_NULL;

/* 线程1 入口*/
static void thread1_mp_alloc(void *parameter)
{
    rt_int32_t i;
    for (i = 0 ; i < 50 ; i++) {
        if (ptr[i] == RT_NULL) {
            /* 试图申请内存块50 次， 当申请不到内存块时，
            线程1 挂起， 转至线程2 运行*/
            ptr[i] = rt_mp_alloc(&mp, RT_WAITING_FOREVER);

            if (ptr[i] != RT_NULL) {
                rt_kprintf("allocate No.%d\n", i);
            }
        }
    }
}

/* 线程2入口，线程2的优先级比线程1低，应该线程1先获得执行。*/
static void thread2_mp_release(void *parameter)
{
    rt_int32_t i;

    rt_kprintf("thread2 try to release block\n");

    for (i = 0; i < 50 ; i++) {
    /* 释放所有分配成功的内存块*/
        if (ptr[i] != RT_NULL) {
            rt_kprintf("release block %d\n", i);
            rt_mp_free(ptr[i]);
            ptr[i] = RT_NULL;
        }
    }
}

int mempool_sample(void)
{
    /* 初始化内存池对象*/
    rt_mp_init(&mp, "mp1", &mempool[0], sizeof(mempool), 80);

    /* 创建线程1： 申请内存池*/
    tid1 = rt_thread_create("thread1",
                            thread1_mp_alloc,
                            RT_NULL,
                            THREAD_STACK_SIZE,
                            THREAD_PRIORITY,
                            THREAD_TIMESLICE);
    if (tid1 != RT_NULL) {
        rt_thread_startup(tid1);
    }

    /* 创建线程2： 释放内存池*/
    tid2 = rt_thread_create("thread2",
                            thread2_mp_release,
                            RT_NULL,
                            THREAD_STACK_SIZE,
                            THREAD_PRIORITY + 1,
                            THREAD_TIMESLICE);
    if (tid2 != RT_NULL) {
        rt_thread_startup(tid2);
    }

    return 0;
}

MSH_CMD_EXPORT(mempool_sample, mempool sample);