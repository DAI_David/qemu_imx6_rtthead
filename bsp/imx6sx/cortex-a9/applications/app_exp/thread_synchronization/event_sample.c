/*
 * File: event_sample.c
 * Created Date: 2020-05-19
 * Author: David
 * Contact: <david_qv@163.com>
 * 
 * Last Modified: Tuesday May 19th 2020 10:12:32 pm
 * 
 * Copyright (c) 2020 NONE
 * It is never too late to be what you might have been.
 * -----
 * HISTORY:
 * Date      	 By	Comments
 */


#include <rtthread.h>

#define THREAD_PRIORITY 9
#define THREAD_TIMESLICE 5
#define EVENT_FLAG3 (1 << 3)
#define EVENT_FLAG5 (1 << 5)

/* 事 件 控 制 块 */
static struct rt_event event;

ALIGN(RT_ALIGN_SIZE)
static char thread1_stack[1024];
static struct rt_thread thread1;

/* 线 程 1 入 口 函 数 */
static void thread1_recv_event(void *param)
{
    rt_uint32_t e;
    
    /* 第 一 次 接 收 事 件， 事 件 3 或 事 件 5 任 意 一 个 可 以 触 发 线 程 1， 接 收 完 后 清 除 事 件 标 志 */
    if (rt_event_recv(&event, (EVENT_FLAG3 | EVENT_FLAG5),
    RT_EVENT_FLAG_OR | RT_EVENT_FLAG_CLEAR,
    RT_WAITING_FOREVER, &e) == RT_EOK) {
        rt_kprintf("thread1: OR recv event 0x%x\n", e);
    }

    rt_kprintf("thread1: delay 1s to prepare the second event\n");
    rt_thread_mdelay(1000);

    /* 第 二 次 接 收 事 件， 事 件 3 和 事 件 5 均 发 生 时 才 可 以 触 发 线 程 1， 接 收 完 后 清 除 事 件 标
    志 */
    if (rt_event_recv(&event, (EVENT_FLAG3 | EVENT_FLAG5),
    RT_EVENT_FLAG_AND | RT_EVENT_FLAG_CLEAR,
    RT_WAITING_FOREVER, &e) == RT_EOK) {
        rt_kprintf("thread1: AND recv event 0x%x\n", e);
    }

    rt_kprintf("thread1 leave.\n");
}

ALIGN(RT_ALIGN_SIZE)
static char thread2_stack[1024];
static struct rt_thread thread2;

/* 线 程 2 入 口 */
static void thread2_send_event(void *param)
{
    rt_kprintf("thread2: send event3\n");
    rt_event_send(&event, EVENT_FLAG3);
    rt_thread_mdelay(200);

    rt_kprintf("thread2: send event5\n");
    rt_event_send(&event, EVENT_FLAG5);
    rt_thread_mdelay(200);

    rt_kprintf("thread2: send event3\n");
    rt_event_send(&event, EVENT_FLAG3);
    rt_kprintf("thread2 leave.\n");
}
int event_sample(void)
{
    rt_err_t result;

    /* 初 始 化 事 件 对 象 */
    result = rt_event_init(&event, "event", RT_IPC_FLAG_FIFO);
    if (result != RT_EOK) {
        rt_kprintf("init event failed.\n");
        return -1;
    }

    rt_thread_init(&thread1,
                    "thread1",
                    thread1_recv_event,
                    RT_NULL,
                    &thread1_stack[0],
                    sizeof(thread1_stack),
                    THREAD_PRIORITY - 1, THREAD_TIMESLICE);
    rt_thread_startup(&thread1);

    rt_thread_init(&thread2,
                    "thread2",
                    thread2_send_event,
                    RT_NULL,
                    &thread2_stack[0],
                    sizeof(thread2_stack),
                    THREAD_PRIORITY, THREAD_TIMESLICE);
    rt_thread_startup(&thread2);

    return 0;
}

MSH_CMD_EXPORT(event_sample, event sample);