/*
 * File: mutex_sample.c
 * Created Date: 2020-05-19
 * Author: David
 * Contact: <david_qv@163.com>
 * 
 * Last Modified: Tuesday May 19th 2020 9:57:07 pm
 * 
 * Copyright (c) 2020 NONE
 * It is never too late to be what you might have been.
 * -----
 * HISTORY:
 * Date      	 By	Comments
 */


#include <rtthread.h>

#define THREAD_PRIORITY 8
#define THREAD_TIMESLICE 5

/* 指 向 互 斥 量 的 指 针 */
static rt_mutex_t dynamic_mutex = RT_NULL;
static rt_uint8_t number1,number2 = 0;

ALIGN(RT_ALIGN_SIZE)
static char thread1_stack[1024];

static struct rt_thread thread1;
static void rt_thread_entry1(void *parameter)
{
    while (1)  {
        /* 线 程 1 获 取 到 互 斥 量 后， 先 后 对 number1、 number2 进 行 加 1 操 作， 然 后 释 放
        互 斥 量 */
        rt_mutex_take(dynamic_mutex, RT_WAITING_FOREVER);
        number1++;

        rt_thread_mdelay(10);

        number2++;
        rt_mutex_release(dynamic_mutex);
    }
}

ALIGN(RT_ALIGN_SIZE)
static char thread2_stack[1024];
static struct rt_thread thread2;
static void rt_thread_entry2(void *parameter)
{
    while (1) {
        /* 线 程 2 获 取 到 互 斥 量 后， 检 查 number1、 number2 的 值 是 否 相 同， 相 同 则 表 示
        mutex 起 到 了 锁 的 作 用 */
        rt_mutex_take(dynamic_mutex, RT_WAITING_FOREVER);
        if (number1 != number2) {
            rt_kprintf("not protect.number1 = %d, mumber2 = %d \n", number1 ,number2);
        } else {
            rt_kprintf("mutex protect ,number1 = mumber2 is %d\n", number1);
        }
        number1++;
        number2++;
        rt_mutex_release(dynamic_mutex);

        if(number1 > 50){
            break;
        }
    }
}

/* 互 斥 量 示 例 的 初 始 化 */
int mutex_sample(void)
{
    /* 创 建 一 个 动 态 互 斥 量 */
    dynamic_mutex = rt_mutex_create("dmutex", RT_IPC_FLAG_FIFO);
    if (dynamic_mutex == RT_NULL) {
        rt_kprintf("create dynamic mutex failed.\n");
        return -1;
    }

    rt_thread_init(&thread1,
                    "thread1",
                    rt_thread_entry1,
                    RT_NULL,
                    &thread1_stack[0],
                    sizeof(thread1_stack),
                    THREAD_PRIORITY, THREAD_TIMESLICE);
    rt_thread_startup(&thread1);

    rt_thread_init(&thread2,
                    "thread2",
                    rt_thread_entry2,
                    RT_NULL,
                    &thread2_stack[0],
                    sizeof(thread2_stack),
                    THREAD_PRIORITY-1, THREAD_TIMESLICE);
    rt_thread_startup(&thread2);

    return 0;
}

MSH_CMD_EXPORT(mutex_sample, mutex sample);