/*
 * File: semaphore_sample.c
 * Created Date: 2020-05-19
 * Author: David
 * Contact: <david_qv@163.com>
 * 
 * Last Modified: Tuesday May 19th 2020 9:00:12 pm
 * 
 * Copyright (c) 2020 NONE
 * It is never too late to be what you might have been.
 * -----
 * HISTORY:
 * Date      	 By	Comments
 */


#include <rtthread.h>

#define THREAD_PRIORITY     25
#define THREAD_TIMESLICE    5

static rt_sem_t dynamic_sem = RT_NULL;
ALIGN(RT_ALIGN_SIZE)
static rt_int8_t thread1_stack[1024];
static struct rt_thread thread1;

static void rt_thread1_entry(void *parameter)
{
    static rt_uint8_t count = 0;
    while (1) {
        if (count <= 100) {
            count++;
        } else {
            return;
        }

        /* count 每 计 数 10 次， 就 释 放 一 次 信 号 量 */
        if (0 == (count % 10)) {
            rt_kprintf("t1 release a dynamic semaphore.\n");
            rt_sem_release(dynamic_sem);
        }
    }
}

ALIGN(RT_ALIGN_SIZE)
static char thread2_stack[1024];
static struct rt_thread thread2;
static void rt_thread2_entry(void *parameter)
{
    static rt_err_t result;
    static rt_uint8_t number = 0;
    while(1) {
        /* 永 久 方 式 等 待 信 号 量， 获 取 到 信 号 量， 则 执 行 number 自 加 的 操 作 */
        result = rt_sem_take(dynamic_sem, RT_WAITING_FOREVER);
        if (result != RT_EOK) {
            rt_kprintf("t2 take a dynamic semaphore, failed.\n");
            rt_sem_delete(dynamic_sem);
            return;
        } else {
            number++;
            rt_kprintf("t2 take a dynamic semaphore. number = %d\n" ,number);
        }
    }
}

/* 信 号 量 示 例 的 初 始 化 */
int semaphore_sample(void)
{
    /* 创 建 一 个 动 态 信 号 量， 初 始 值 是 0 */
    dynamic_sem = rt_sem_create("dsem", 0, RT_IPC_FLAG_FIFO);
    if (dynamic_sem == RT_NULL) {
        rt_kprintf("create dynamic semaphore failed.\n");
        return -1;
    } else {
        rt_kprintf("create done. dynamic semaphore value = 0.\n");
    }

    rt_thread_init(&thread1,
                    "thread1",
                    rt_thread1_entry,
                    RT_NULL,
                    &thread1_stack[0],
                    sizeof(thread1_stack),
                    THREAD_PRIORITY, THREAD_TIMESLICE);

    rt_thread_startup(&thread1);

    rt_thread_init(&thread2,
                    "thread2",
                    rt_thread2_entry,
                    RT_NULL,
                    &thread2_stack[0],
                    sizeof(thread2_stack),
                    THREAD_PRIORITY-1, THREAD_TIMESLICE);

    rt_thread_startup(&thread2);

    return 0;
}

MSH_CMD_EXPORT(semaphore_sample, semaphore sample);