/*
 * File: scheduler_hook.c
 * Created Date: 2020-05-19
 * Author: David
 * Contact: <david_qv@163.com>
 * 
 * Last Modified: Tuesday May 19th 2020 8:51:04 pm
 * 
 * Copyright (c) 2020 NONE
 * It is never too late to be what you might have been.
 * -----
 * HISTORY:
 * Date      	 By	Comments
 */


#include <rtthread.h>

#define THREAD_STACK_SIZE	1024
#define THREAD_PRIORITY		20
#define THREAD_TIMESLICE	10

volatile rt_uint32_t count[2];
static rt_thread_t tid1 = RT_NULL;
static rt_thread_t tid2 = RT_NULL;

static void thread_entry(void *param)
{
    rt_uint32_t value;

    value = (rt_uint32_t)param;
    while (1) {
        rt_kprintf("thread %d is running\n", value);
        rt_thread_mdelay(1000);
    }
}

static void hook_of_scheduler(rt_thread_t from, rt_thread_t to)
{
    rt_kprintf("from: %s --> to: %s \n", from->name, to->name);
}

int scheduler_hook(void)
{
    rt_scheduler_sethook(hook_of_scheduler);

    tid1 = rt_thread_create("thread1",
                        thread_entry,
                        (void*)1,
                        THREAD_STACK_SIZE,
                        THREAD_PRIORITY,
                        THREAD_TIMESLICE);
    if (tid1 != RT_NULL) {
        rt_thread_startup(tid1);
    }

    tid2 = rt_thread_create("thread2",
                        thread_entry,
                        (void*)2,
                        THREAD_STACK_SIZE,
                        THREAD_PRIORITY,
                        THREAD_TIMESLICE);
    if (tid2 != RT_NULL) {
        rt_thread_startup(tid2);
    }

    return 0;
}

MSH_CMD_EXPORT(scheduler_hook, scheduler_hook sample);