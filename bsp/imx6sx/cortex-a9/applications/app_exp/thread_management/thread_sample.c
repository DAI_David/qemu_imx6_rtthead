/*
 * File: thread_sample.c
 * Created Date: 2020-05-19
 * Author: David
 * Contact: <david_qv@163.com>
 * 
 * Last Modified: Tuesday May 19th 2020 8:51:13 pm
 * 
 * Copyright (c) 2020 NONE
 * It is never too late to be what you might have been.
 * -----
 * HISTORY:
 * Date      	 By	Comments
 */


#include <rtthread.h>

#define THREAD_PRIORITY		25
#define THREAD_STACK_SIZE	512
#define THREAD_TIMESLICE	5

static rt_thread_t tid1	= RT_NULL;
ALIGN(RT_ALIGN_SIZE)
static struct rt_thread thread2;
static rt_int8_t thread2_stack[1024];

static void thread1_entry(void *param)
{
    rt_uint32_t count = 0;
    while (1) {
        rt_kprintf("thread1 count: %d\n", count ++);
        rt_thread_mdelay(500);
    }
}

static void thread2_entry(void *param)
{
    rt_uint32_t count = 0;

    for (count = 0; count < 10; count++) {
        rt_kprintf("thread2 count: %d\n", count);
    }
    rt_kprintf("thread2_exit\n");
}

int thread_sample(void)
{
    tid1 = rt_thread_create("thread1",
                            thread1_entry,
                            RT_NULL,
                            THREAD_STACK_SIZE,
                            THREAD_PRIORITY,
                            THREAD_TIMESLICE);
    if (tid1 != RT_NULL) {
        rt_thread_startup(tid1);
    }

    rt_thread_init(&thread2,
                    "thread2",
                    thread2_entry,
                    RT_NULL,
                    thread2_stack,
                    sizeof(thread2_stack),
                    THREAD_PRIORITY - 1,
                    THREAD_TIMESLICE);
    rt_thread_startup(&thread2);

    return 0;
}

MSH_CMD_EXPORT(thread_sample, thread sample);

