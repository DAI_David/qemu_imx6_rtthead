/*
 * File: signal_sample.c
 * Created Date: 2020-05-20
 * Author: David
 * Contact: <david_qv@163.com>
 * 
 * Last Modified: Wednesday May 20th 2020 1:52:36 pm
 * 
 * Copyright (c) 2020 NONE
 * It is never too late to be what you might have been.
 * -----
 * HISTORY:
 * Date      	 By	Comments
 */


#include <rtthread.h>

#define THREAD_PRIORITY 25
#define THREAD_STACK_SIZE 512
#define THREAD_TIMESLICE 5

static rt_thread_t tid1 = RT_NULL;

/* 线程1 的信号处理函数*/
void thread1_signal_handler(int sig)
{
    rt_kprintf("thread1 received signal %d\n", sig);
}

/* 线程1 的入口函数*/
static void thread1_entry(void *parameter)
{
    rt_int32_t cnt = 0;

    /* 安装信号*/
    rt_signal_install(SIGUSR1, thread1_signal_handler);
    rt_signal_unmask(SIGUSR1);

    /* 运行10 次*/
    while (cnt < 10) {
        /* 线程1 采用低优先级运行， 一直打印计数值*/
        rt_kprintf("thread1 count : %d\n", cnt);
        cnt++;

        rt_thread_mdelay(100);
    }
}

/* 信号示例的初始化*/
int signal_sample(void)
{
    /* 创建线程1 */
    tid1 = rt_thread_create("thread1",
                            thread1_entry, RT_NULL,
                            THREAD_STACK_SIZE,
                            THREAD_PRIORITY,
                            THREAD_TIMESLICE);
    if (tid1 != RT_NULL) {
        rt_thread_startup(tid1);
    }

    rt_thread_mdelay(300);

    /* 发送信号SIGUSR1 给线程1 */
    rt_thread_kill(tid1, SIGUSR1);

    return 0;
}

MSH_CMD_EXPORT(signal_sample, signal sample);