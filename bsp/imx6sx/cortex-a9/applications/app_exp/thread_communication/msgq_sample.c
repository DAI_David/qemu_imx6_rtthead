/*
 * File: msgq_sample.c
 * Created Date: 2020-05-20
 * Author: David
 * Contact: <david_qv@163.com>
 * 
 * Last Modified: Wednesday May 20th 2020 1:20:19 pm
 * 
 * Copyright (c) 2020 NONE
 * It is never too late to be what you might have been.
 * -----
 * HISTORY:
 * Date      	 By	Comments
 */


#include <rtthread.h>

/* 消息队列控制块*/
static struct rt_messagequeue mq;

/* 消息队列中用到的放置消息的内存池*/
static rt_uint8_t msg_pool[2048];

ALIGN(RT_ALIGN_SIZE)
static char thread1_stack[1024];
static struct rt_thread thread1;

/* 线程1 入口函数*/
static void thread1_entry(void *parameter)
{
    rt_int8_t buf = 0;
    rt_uint8_t cnt = 0;

    while (1) {
        /* 从消息队列中接收消息*/
        if (rt_mq_recv(&mq, &buf, sizeof(buf), RT_WAITING_FOREVER) == RT_EOK) {
            rt_kprintf("thread1: recv msg from msg queue, the content:%c\n", buf);

        if (cnt++ == 19) {
            break;
        }
    }

        /* 延时50ms */
        rt_thread_mdelay(50);
    }

    rt_kprintf("thread1: detach mq \n");
    rt_mq_detach(&mq);
}

ALIGN(RT_ALIGN_SIZE)
static char thread2_stack[1024];
static struct rt_thread thread2;

/* 线程2 入口*/
static void thread2_entry(void *parameter)
{
    rt_int32_t result;
    rt_int8_t buf = 'A';
    rt_uint8_t cnt = 0;

    while (1) {
        if (cnt == 8) {
            /* 发送紧急消息到消息队列中*/
            result = rt_mq_urgent(&mq, &buf, 1);

            if (result != RT_EOK) {
                rt_kprintf("rt_mq_urgent ERR\n");
            } else {
                rt_kprintf("thread2: send urgent message - %c\n", buf);
            }
        } else if (cnt>= 20)/* 发送20 次消息之后退出*/ {
            rt_kprintf("message queue stop send, thread2 quit\n");
            break;
        } else {
            /* 发送消息到消息队列中*/
            result = rt_mq_send(&mq, &buf, 1);
            if (result != RT_EOK) {
                rt_kprintf("rt_mq_send ERR\n");
            }

            rt_kprintf("thread2: send message - %c\n", buf);
        }

        buf++;
        cnt++;

        /* 延时5ms */
        rt_thread_mdelay(5);
    }
}

/* 消息队列示例的初始化*/
int msgq_sample(void)
{
    rt_err_t result;

    /* 初始化消息队列*/
    result = rt_mq_init(&mq,
                        "mqt",
                        &msg_pool[0], /* 内存池指向msg_pool */
                        1, /* 每个消息的大小是1 字节*/
                        sizeof(msg_pool), /* 内存池的大小是msg_pool 的大小
                        */
                        RT_IPC_FLAG_FIFO); /* 如果有多个线程等待， 按照先来先得到的方法分配消息*/
    if (result != RT_EOK) {
        rt_kprintf("init message queue failed.\n");
        return -1;
    }

    rt_thread_init(&thread1,
                    "thread1",
                    thread1_entry,
                    RT_NULL,
                    &thread1_stack[0],
                    sizeof(thread1_stack), 25, 5);
    rt_thread_startup(&thread1);

    rt_thread_init(&thread2,
                    "thread2",
                    thread2_entry,
                    RT_NULL,
                    &thread2_stack[0],
                    sizeof(thread2_stack), 25, 5);
    rt_thread_startup(&thread2);

    return 0;
}

MSH_CMD_EXPORT(msgq_sample, msgq sample);