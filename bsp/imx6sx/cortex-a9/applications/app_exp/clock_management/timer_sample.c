/*
 * File: timer_sample.c
 * Created Date: 2020-05-19
 * Author: David
 * Contact: <david_qv@163.com>
 * 
 * Last Modified: Tuesday May 19th 2020 8:50:38 pm
 * 
 * Copyright (c) 2020 NONE
 * It is never too late to be what you might have been.
 * -----
 * HISTORY:
 * Date      	 By	Comments
 */


#include <rtthread.h>

#define TICK_TIME   	10

static rt_timer_t timer1 = RT_NULL;
static rt_timer_t timer2 = RT_NULL;
static int cnt = 0;

static void timeout1(void *param)
{
    rt_kprintf("periodic timer is timeout %d\n", cnt);

    if (cnt++ >= 9)
    {
        rt_timer_stop(timer1);
        rt_kprintf("periodic timer was stopped!\n");
    }
}

static void timeout2(void *param)
{
    rt_kprintf("one shot timer is timeout\n");
}

int timer_sample(void)
{
    timer1 = rt_timer_create("timer1",
                            timeout1,
                            RT_NULL,
                            TICK_TIME,
                            RT_TIMER_FLAG_PERIODIC);
    if (timer1 != RT_NULL) {
        rt_timer_start(timer1);
    }

    timer2 = rt_timer_create("timer2",
                            timeout2,
                            RT_NULL,
                            TICK_TIME * 3,
                            RT_TIMER_FLAG_PERIODIC);
    if (timer2 != RT_NULL) {
        rt_timer_start(timer2);
    }

    return 0;
}

MSH_CMD_EXPORT(timer_sample, timer sample);
